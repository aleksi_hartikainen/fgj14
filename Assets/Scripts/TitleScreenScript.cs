﻿using UnityEngine;
using System.Collections;

public class TitleScreenScript : MonoBehaviour 
{
    public GUIStyle guiStyle;
    public Texture PlayButtonTexture;
    public SpriteRenderer spriteRenderer;
    private int count = 0;
    private int disturbanceFrequency = 1;

	void Start () 
    {
        //this.spriteRenderer = GetComponent<SpriteRenderer>();
	}
	
	void FixedUpdate () 
    {
        if (((int)(Time.time)) % 4 == 0)
        {
            if (count == disturbanceFrequency)
            {
                this.spriteRenderer.material.color = new Color(1 - Random.value * 0.2f, 1 - Random.value * 0.2f, 1 - Random.value * 0.2f);
            }
            count++;
            if (count > disturbanceFrequency)
            {
                count = 0;
            }
        }

	}

    private void OnGUI() 
    {
        if (GUI.Button(new Rect(Screen.width * 0.5f - 175, Screen.height * 0.6f, 350, 60), this.PlayButtonTexture, this.guiStyle))
        {
            Application.LoadLevel("Scene1");
        }
    }
}
