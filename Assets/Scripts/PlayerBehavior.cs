﻿using UnityEngine;
using System.Collections;

public class PlayerBehavior : MonoBehaviour 
{
    private Transform myTransform;
    public float moveSpeed;
    private float angle;
    private bool moving = false;
    private bool targetPositionReached = false;
    public float health;
    public float maxHealth;

    public Sprite brainDamage; 

    public bool isAlive = true;
    public bool isInPain = false;

    public AudioClip walkSound;
    public AudioClip burnSound;

    public AudioSource moveAudioSource;
    public AudioSource burnAudioSource;
    public AudioSource deathAudioSource;

    public Animator animator;
    public LightControl[] lights;

    public ParticleSystem painParticleSystem;

    public AudioClip[] deathSounds; 


	void Start () 
    {
        this.myTransform = transform;
        this.moveAudioSource.clip = this.walkSound;
        this.moveAudioSource.loop = true;
        this.burnAudioSource.clip = this.burnSound;
        this.burnAudioSource.loop = true;

        this.deathAudioSource.loop = false;
        //this.brainDamage.
    }

    void FixedUpdate()
    {
        if (this.isAlive)
        {
            if (Input.GetMouseButton(0) || Input.touchCount > 0)
            {
                Vector3 mouseWorldPoint = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                if (Input.touchCount > 0)
                {
                    mouseWorldPoint = Camera.main.ScreenToWorldPoint(Input.touches[0].position);
                    if (Input.touchCount == 1)
                    {
                        this.moveSpeed = 10;
                    }
                    else
                    {
                        this.moveSpeed = 5;
                    }
                }
                Vector2 directionVector = mouseWorldPoint - myTransform.position;

                if (directionVector.magnitude > 0.5f)
                {
                    rigidbody2D.velocity = directionVector.normalized * moveSpeed;
                    this.angle = Mathf.Atan2(directionVector.y, directionVector.x) * Mathf.Rad2Deg;
                    this.moving = true;
                    if (!this.moveAudioSource.isPlaying)
                    {
                        this.moveAudioSource.timeSamples = (int)(this.walkSound.samples * Random.value);              
                        this.moveAudioSource.Play();
                    }
                }
                else
                {
                    rigidbody2D.velocity = Vector2.zero;
                    this.moveAudioSource.Stop();
                    this.moving = false;
                }
            }
            else
            {
                this.moveAudioSource.Stop();
                this.burnAudioSource.Stop();
                rigidbody2D.velocity = Vector2.zero;
                this.moving = false;
            }
        }

        if (this.lights[0].LightsPoint(this.myTransform.position))
        {
            this.isInPain = true;         
            //if (!this.painParticleSystem.isPlaying)
            //{
                //print("play particle now!");
                this.painParticleSystem.Play();
                this.health -= Time.deltaTime;
            //}

            if (!this.burnAudioSource.isPlaying)
            {
                this.burnAudioSource.timeSamples = (int)(this.burnSound.samples * Random.value);
                this.burnAudioSource.Play();
            }
        }
        else
	    {
            this.isInPain = false;
            this.burnAudioSource.Stop();
            this.painParticleSystem.Stop();
	    }

        if (this.health < 0)
        {
            rigidbody2D.velocity = Vector2.zero;

            if (this.isAlive)
            {
                this.deathAudioSource.PlayOneShot(this.deathSounds[Random.Range(0, this.deathSounds.Length)]);
            }
            this.isAlive = false;             
            this.animator.SetBool("IsDead", true);
            this.painParticleSystem.Stop();
            this.moveAudioSource.Stop();
            this.burnAudioSource.Stop();
        }
        
        this.animator.SetBool("InPain", this.isInPain);
        this.animator.SetBool("Moving", this.moving);      

        myTransform.rotation = Quaternion.Euler(0, 0, angle - 90);
    }

    private void OnGUI()
    {
        if (!this.isAlive)
        {
            if (GUI.Button(new Rect(Screen.width * 0.5f, Screen.height * 0.5f, 70, 35), "Restart"))
            {
                Application.LoadLevel(Application.loadedLevelName);
            }
        }
    }
}
