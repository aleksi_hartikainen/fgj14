﻿using UnityEngine;
using System.Collections;

public class CameraBehavior : MonoBehaviour 
{
    public PlayerBehavior playerBehavior;
    private Transform myTransform;

    public SpriteRenderer spriteRenderer;
    private float colorVariation;
 
	void Start () 
    {
        this.myTransform = transform;
	}
	
	void FixedUpdate () 
    {
        if (this.playerBehavior.isInPain && this.playerBehavior.isAlive)
        {
            this.myTransform.position = new Vector3(Random.value * 0.05f, Random.value * 0.05f, -10);
            colorVariation = Random.value;
            this.spriteRenderer.material.color = new Color(1, 1 - colorVariation * 0.2f, 1 - colorVariation * 0.2f);
        }
        else
        {
            this.myTransform.position = new Vector3(0, 0, -10);
            this.spriteRenderer.material.color = Color.white;
        }
	}
}
