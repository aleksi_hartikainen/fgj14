﻿using UnityEngine;
using System.Collections;

public class LightControl : MonoBehaviour {
	GameObject[] boxes;
	Mesh shadowMesh;
	Vector3[] shadowCorners;
	Vector2[][] colliderPoints;
	GameObject shadow;
	PolygonCollider2D[] polyColliders;
	// Use this for initialization
	void Start () {
		GameObject world = GameObject.Find ("World");
		shadow = this.transform.FindChild ("Shadow").gameObject;

		int boxCount = world.transform.childCount;
		boxes = new GameObject[world.transform.childCount];
		int k = 0;
		foreach (Transform child in world.transform) {
			boxes[k++] = child.gameObject;
		}
		shadowMesh = new Mesh ();
		shadowCorners = new Vector3[boxCount * 4];
		colliderPoints = new Vector2[boxCount][];
		polyColliders = new PolygonCollider2D[boxCount]; 

		for (int i = 0; i < boxCount; ++i) {
			polyColliders[i] = this.gameObject.AddComponent(typeof(PolygonCollider2D)) as PolygonCollider2D;
			polyColliders[i].isTrigger = true;
			Debug.Log(polyColliders[i]);
			colliderPoints [i] = new Vector2[4];
		}

		Vector3[] normals = new Vector3[4 * boxCount];
		Vector4[] tangent = new Vector4[4 * boxCount];
		for (int j = 0; j < boxCount; ++j) {
			for (int i = 0; i < 4; ++i) {
				normals [i + j * 4] = new Vector3 (0, 0, -1);
				tangent [i + j * 4] = new Vector4 (1, 0, 0, 1);
			}
		}
		
		shadowMesh.vertices = shadowCorners;
		shadowMesh.normals = normals;
		shadowMesh.tangents = tangent;
		MeshFilter filt = shadow.GetComponent (typeof(MeshFilter)) as MeshFilter;
		shadowMesh.name = "DynamicShadowMesh";
		int[] indices = new int[6 * boxCount];
		k = 0;
		for (int i = 0; i < boxCount; ++i) {
			indices[k++] = i * 4 + 0;
			indices[k++] = i * 4 + 1;
			indices[k++] = i * 4 + 3;
			indices[k++] = i * 4 + 3;
			indices[k++] = i * 4 + 1;
			indices[k++] = i * 4 + 2;
			// {0, 1, 2, 2, 1, 3};
		}
		shadowMesh.triangles = indices;
		filt.mesh = shadowMesh;
		MeshRenderer renderer = shadow.GetComponent (typeof(MeshRenderer)) as MeshRenderer;
		Material mat = renderer.material;
		Material newMat = new Material (mat);
		newMat.shader = mat.shader;
		newMat.CopyPropertiesFromMaterial (mat);
		newMat.renderQueue = 3002;
		renderer.material = newMat;
		shadow.transform.position = shadow.transform.position + new Vector3(0,0, -0.01f);
	}

	void Update() {
		for (int j = 0; j < boxes.Length; ++j) 
		{
			GameObject box = boxes[j];
			var size = (box.collider2D as BoxCollider2D).size / 2;
			size.Scale (box.transform.localScale);
			var gsize  = size;
			Vector3 pos = box.transform.position - this.transform.position;
			pos.z = 0;
			Vector3 lightPos3 = new Vector3(0,0,0);
			Vector2 lightPos = new Vector2(lightPos3.x, lightPos3.y);
			
			int start = j * 4;
			if (lightPos.x > pos.x + gsize.x) {
				if (lightPos.y > pos.y + gsize.y) {
					shadowCorners [start + 0] = pos + new Vector3 (-size.x, +size.y, 0.0f);
					shadowCorners [start + 1] = pos + new Vector3 (+size.x, -size.y, 0.0f);
				} else if (lightPos.y < pos.y - gsize.y) {
					shadowCorners [start + 0] = pos + new Vector3 (+size.x, +size.y, 0.0f);
					shadowCorners [start + 1] = pos + new Vector3 (-size.x, -size.y, 0.0f);
				} else {
					shadowCorners [start + 0] = pos + new Vector3 (+size.x, +size.y, 0.0f);
					shadowCorners [start + 1] = pos + new Vector3 (+size.x, -size.y, 0.0f);
				}
			} else if (lightPos.x < pos.x - gsize.x) {
				if (lightPos.y > pos.y + gsize.y) {
					shadowCorners [start + 0] = pos + new Vector3 (-size.x, -size.y, 0.0f);
					shadowCorners [start + 1] = pos + new Vector3 (+size.x, +size.y, 0.0f);
					
				} else if (lightPos.y < pos.y - gsize.y) {
					shadowCorners [start + 1] = pos + new Vector3 (-size.x, +size.y, 0.0f);
					shadowCorners [start + 0] = pos + new Vector3 (+size.x, -size.y, 0.0f);
				} else {
					shadowCorners [start + 1] = pos + new Vector3 (-size.x, +size.y, 0.0f);
					shadowCorners [start + 0] = pos + new Vector3 (-size.x, -size.y, 0.0f);
				}
			} else {
				if (lightPos.y < pos.y) {
					shadowCorners [start + 0] = pos + new Vector3 (size.x, -size.y, 0.0f);
					shadowCorners [start + 1] = pos + new Vector3 (-size.x, -size.y, 0.0f);
				} else {
					shadowCorners [start + 1] = pos + new Vector3 (size.x, +size.y, 0.0f);
					shadowCorners [start + 0] = pos + new Vector3 (-size.x, +size.y, 0.0f);
				}
			}
			shadowCorners[start + 3] = 100 * shadowCorners[start + 0].normalized;
			shadowCorners[start + 2] = 100 * shadowCorners[start + 1].normalized;

			for (int z = 0; z < 4; ++z) {
				colliderPoints[j][z] = shadowCorners[start + z];
			}

			polyColliders[j].SetPath(0, colliderPoints[j]);

		}
		shadowMesh.vertices = shadowCorners;
	}

	public bool LightsPoint(Vector2 p) {
		foreach (PolygonCollider2D col in this.polyColliders) {
			if (col.OverlapPoint(p)) return false;
		}
		return true;
	}

	// Update is called once per frame
	void FixedUpdate () {
		this.transform.position += new Vector3 (Mathf.Sin (Time.time), Mathf.Cos (Time.time),0) * 0.001f;
		var p = this.transform.position;
		if (Input.GetKey (KeyCode.A)) {
			this.transform.position = p - new Vector3(0.1f,0,0);
		}
		if (Input.GetKey (KeyCode.S)) {
			this.transform.position = p - new Vector3(0.0f,0.1f,0);
		}
		if (Input.GetKey (KeyCode.D)) {
			this.transform.position = p + new Vector3(0.1f,0,0);
		}
		if (Input.GetKey (KeyCode.W)) {
			this.transform.position = p + new Vector3(0,0.1f,0);
		}
	}
}
