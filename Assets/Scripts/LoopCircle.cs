﻿using UnityEngine;
using System.Collections;

public class LoopCircle : MonoBehaviour {

	public Vector2[] points = {
		new Vector2 (-8, -4),
		new Vector2 (-8, 4),
		new Vector2 (8, 4),
		new Vector2 (8, -4)
	};

	Transform myTransform;
	// Use this for initialization
	void Start () {
		p = 1;
		myTransform = this.transform;
		myTransform.position = new Vector3 (points [0].x, points [0].y, myTransform.position.z);

	}

	int p = 0;
	float phase = 0.0f;
	public float speed = 0.5f;
	void FixedUpdate () {
		Vector2 pos = myTransform.position;
		Vector2 x = points [p];
		Vector2 dir = x - pos;
		if (dir.magnitude < 2 * speed * Time.fixedDeltaTime) {
				x = points [p];

				p ++;
				p = p % points.Length;
				myTransform.position = new Vector3 (x.x, x.y, myTransform.position.z);
		} else {
			dir.Normalize();
			Vector2 np = pos + dir * speed * Time.fixedDeltaTime;
			myTransform.position = new Vector3(np.x, np.y, myTransform.position.z);
		}

		

	}
}
