﻿Shader "Custom/Shadow" {
	Properties {
		_Color ("Main Color", Color) = (1,1,1,0)
		_Level ("Level", int) = 1
	}
	SubShader {		
		Blend SrcAlpha OneMinusSrcAlpha
		Cull Off
		ZTest Less
		
		CGPROGRAM
		#pragma surface surf Lambert
		float4 _Color;
		struct Input {
			float4 color : COLOR;
		};
		void surf (Input IN, inout SurfaceOutput o) {
			o.Albedo = _Color.rgb;
			o.Normal = half3(0,0,-1);
			o.Alpha = _Color.a;
		}
		ENDCG
	
	}
}
