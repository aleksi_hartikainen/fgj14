﻿using UnityEngine;
using System.Collections;

public class LogoMove : MonoBehaviour 
{
    private Transform myTransform;
    public float keikkuAmount, keikkuPerSecond;
    private Quaternion startRotation;

    void Start () 
    {
        this.myTransform = transform;
        startRotation = this.transform.localRotation;
	}
	
	void Update () 
    {
        this.myTransform.localRotation = startRotation;
        this.myTransform.RotateAround
        (
            new Vector3(1.0f, 1.0f, 1.0f),
            keikkuAmount * (Mathf.Sin(Mathf.Lerp(0.0f, Mathf.PI * 2.0f, (Time.time * keikkuPerSecond - Mathf.Floor(Time.time * keikkuPerSecond)))) * 2.0f - 1.0f)
        );
	}
}
